import 'dart:io';

import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:stokkur_api_client/src/model/api_error_converter.dart';

part 'api_exception.freezed.dart';

@freezed
class ApiException<T> with _$ApiException<T> {
  const factory ApiException.requestCancelled(T error) = RequestCancelled;
  const factory ApiException.unauthorisedRequest(T error) = UnauthorisedRequest;
  const factory ApiException.badRequest(T error) = BadRequest;
  const factory ApiException.forbidden(T error) = Forbidden;
  const factory ApiException.notFound(T error) = NotFound;
  const factory ApiException.methodNotAllowed(T error) = MethodNotAllowed;
  const factory ApiException.notAcceptable(T error) = NotAcceptable;
  const factory ApiException.requestTimeout(T error) = RequestTimeout;
  const factory ApiException.unprocessableEntity(T error) = UnprocessableEntity;
  const factory ApiException.sendTimeout(T error) = SendTimeout;
  const factory ApiException.conflict(T error) = Conflict;
  const factory ApiException.internalServerError(T error) = InternalServerError;
  const factory ApiException.notImplemented(T error) = NotImplemented;
  const factory ApiException.serviceUnavailable(T error) = ServiceUnavailable;
  const factory ApiException.noInternetConnection(T error) = NoInternetConnection;
  const factory ApiException.formatException(T error) = FormatException;
  const factory ApiException.unableToProcess(T error) = UnableToProcess;
  const factory ApiException.defaultError(T error) = DefaultError;
  const factory ApiException.unexpectedError(T error) = UnexpectedError;
  const factory ApiException.badCertificate(T error) = BadCerfiticate;
  const factory ApiException.connectionError(T error) = ConnectionError;

  static ApiException<T> getDioException<T>(error, ApiErrorConverter converter) {
    if (error is Exception) {
      try {
        late ApiException<T> networkExceptions;
        if (error is DioError) {
          final errorData = converter.convertError(error.response?.data);
          switch (error.type) {
            case DioErrorType.cancel:
              networkExceptions = ApiException.requestCancelled(errorData);
              break;
            case DioErrorType.connectionTimeout:
              networkExceptions = ApiException.requestTimeout(errorData);
              break;
            case DioErrorType.unknown:
              networkExceptions = ApiException.noInternetConnection(errorData);
              break;
            case DioErrorType.receiveTimeout:
              networkExceptions = ApiException.sendTimeout(errorData);
              break;
            case DioErrorType.badResponse:
              switch (error.response?.statusCode) {
                case 400:
                  networkExceptions = ApiException.badRequest(errorData);
                  break;
                case 401:
                  networkExceptions = ApiException.unauthorisedRequest(errorData);
                  break;
                case 403:
                  networkExceptions = ApiException.forbidden(errorData);
                  break;
                case 404:
                  networkExceptions = ApiException.notFound(errorData);
                  break;
                case 409:
                  networkExceptions = ApiException.conflict(errorData);
                  break;
                case 408:
                  networkExceptions = ApiException.requestTimeout(errorData);
                  break;
                case 422:
                  networkExceptions = ApiException.unprocessableEntity(errorData);
                  break;
                case 500:
                  networkExceptions = ApiException.internalServerError(errorData);
                  break;
                case 503:
                  networkExceptions = ApiException.serviceUnavailable(errorData);
                  break;
                default:
                  var responseCode = error.response?.statusCode;
                  networkExceptions = ApiException.defaultError(
                    converter.convertError("Received invalid status code: $responseCode"),
                  );
              }
              break;
            case DioErrorType.sendTimeout:
              networkExceptions = ApiException.sendTimeout(errorData);
              break;
            case DioErrorType.badCertificate:
              networkExceptions = ApiException.badCertificate(errorData);
              break;
            case DioErrorType.connectionError:
              networkExceptions = ApiException.connectionError(errorData);
              break;
          }
        } else if (error is SocketException) {
          networkExceptions = ApiException.noInternetConnection(converter.convertError("No internet"));
        } else {
          networkExceptions = ApiException.unexpectedError(converter.convertError("Unexpected error"));
        }
        return networkExceptions;
      } on FormatException catch (_) {
        return ApiException.formatException(converter.convertError("Format exception"));
      } catch (_) {
        return ApiException.unexpectedError(converter.convertError("Unexpected error"));
      }
    } else {
      if (error.toString().contains("is not a subtype of")) {
        return ApiException.unableToProcess(error.response.data);
      } else {
        return ApiException.unexpectedError(error.response.data);
      }
    }
  }

  static String getErrorMessage(ApiException networkExceptions, {String? language = "en"}) {
    return networkExceptions.when(
        notImplemented: (_) => "Not Implemented",
        requestCancelled: (_) => "Request Cancelled",
        internalServerError: (_) => "Internal Server Error",
        notFound: (_) => "Not found",
        serviceUnavailable: (_) => "Service unavailable",
        methodNotAllowed: (_) => "Method Allowed",
        badRequest: (_) => "Bad request",
        forbidden: (_) => "Forbidden",
        unauthorisedRequest: (_) => "Unauthorised request",
        unexpectedError: (_) => "Unexpected error occurred",
        requestTimeout: (_) => "Connection request timeout",
        unprocessableEntity: (_) => "Unprocessable entity",
        conflict: (_) => "Error due to a conflict",
        sendTimeout: (_) => "Send timeout in connection with API server",
        unableToProcess: (_) => "Unable to process the data",
        defaultError: (error) => error,
        formatException: (_) => "Unexpected error occurred",
        notAcceptable: (_) => "Not acceptable",
        badCertificate: (_) => "Bad certificate",
        connectionError: (_) => "Connection error",
        noInternetConnection: (_) => language == "is"
            ? "Engin nettenging fannst, vinsamlegast reyndu aftur síðast"
            : "You are not connected to any network, please try again later");
  }

  static String getCause(ApiException exception) {
    return exception.error;
  }
}
