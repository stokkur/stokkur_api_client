abstract class ApiErrorConverter<T> {
  T convertError(dynamic error);
}

class DefaultErrorConverter implements ApiErrorConverter<String> {
  @override
  String convertError(dynamic error) {
    switch (error.runtimeType) {
      case String:
        return error;
      default:
        {
          try {
            return _convertJsonMapToString(Map.from(error)) ?? "";
          } catch (_) {
            return "";
          }
        }
    }
  }

  String? _convertJsonMapToString(Map<String, dynamic> map) {
    if (map.containsKey("data")) {
      map = map["data"];
    }

    if (map.containsKey("error")) {
      return map["error"];
    }
    return null;
  }
}
