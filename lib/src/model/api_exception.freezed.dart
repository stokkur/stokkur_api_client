// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'api_exception.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ApiException<T> {
  T get error => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ApiExceptionCopyWith<T, ApiException<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ApiExceptionCopyWith<T, $Res> {
  factory $ApiExceptionCopyWith(
          ApiException<T> value, $Res Function(ApiException<T>) then) =
      _$ApiExceptionCopyWithImpl<T, $Res, ApiException<T>>;
  @useResult
  $Res call({T error});
}

/// @nodoc
class _$ApiExceptionCopyWithImpl<T, $Res, $Val extends ApiException<T>>
    implements $ApiExceptionCopyWith<T, $Res> {
  _$ApiExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_value.copyWith(
      error: freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RequestCancelledCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$RequestCancelledCopyWith(_$RequestCancelled<T> value,
          $Res Function(_$RequestCancelled<T>) then) =
      __$$RequestCancelledCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$RequestCancelledCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$RequestCancelled<T>>
    implements _$$RequestCancelledCopyWith<T, $Res> {
  __$$RequestCancelledCopyWithImpl(
      _$RequestCancelled<T> _value, $Res Function(_$RequestCancelled<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$RequestCancelled<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$RequestCancelled<T> implements RequestCancelled<T> {
  const _$RequestCancelled(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.requestCancelled(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RequestCancelled<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RequestCancelledCopyWith<T, _$RequestCancelled<T>> get copyWith =>
      __$$RequestCancelledCopyWithImpl<T, _$RequestCancelled<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return requestCancelled(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return requestCancelled?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (requestCancelled != null) {
      return requestCancelled(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return requestCancelled(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return requestCancelled?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (requestCancelled != null) {
      return requestCancelled(this);
    }
    return orElse();
  }
}

abstract class RequestCancelled<T> implements ApiException<T> {
  const factory RequestCancelled(final T error) = _$RequestCancelled<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$RequestCancelledCopyWith<T, _$RequestCancelled<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnauthorisedRequestCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$UnauthorisedRequestCopyWith(_$UnauthorisedRequest<T> value,
          $Res Function(_$UnauthorisedRequest<T>) then) =
      __$$UnauthorisedRequestCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$UnauthorisedRequestCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$UnauthorisedRequest<T>>
    implements _$$UnauthorisedRequestCopyWith<T, $Res> {
  __$$UnauthorisedRequestCopyWithImpl(_$UnauthorisedRequest<T> _value,
      $Res Function(_$UnauthorisedRequest<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$UnauthorisedRequest<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$UnauthorisedRequest<T> implements UnauthorisedRequest<T> {
  const _$UnauthorisedRequest(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.unauthorisedRequest(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnauthorisedRequest<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UnauthorisedRequestCopyWith<T, _$UnauthorisedRequest<T>> get copyWith =>
      __$$UnauthorisedRequestCopyWithImpl<T, _$UnauthorisedRequest<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return unauthorisedRequest(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return unauthorisedRequest?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (unauthorisedRequest != null) {
      return unauthorisedRequest(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return unauthorisedRequest(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return unauthorisedRequest?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (unauthorisedRequest != null) {
      return unauthorisedRequest(this);
    }
    return orElse();
  }
}

abstract class UnauthorisedRequest<T> implements ApiException<T> {
  const factory UnauthorisedRequest(final T error) = _$UnauthorisedRequest<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$UnauthorisedRequestCopyWith<T, _$UnauthorisedRequest<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$BadRequestCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$BadRequestCopyWith(
          _$BadRequest<T> value, $Res Function(_$BadRequest<T>) then) =
      __$$BadRequestCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$BadRequestCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$BadRequest<T>>
    implements _$$BadRequestCopyWith<T, $Res> {
  __$$BadRequestCopyWithImpl(
      _$BadRequest<T> _value, $Res Function(_$BadRequest<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$BadRequest<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$BadRequest<T> implements BadRequest<T> {
  const _$BadRequest(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.badRequest(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BadRequest<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BadRequestCopyWith<T, _$BadRequest<T>> get copyWith =>
      __$$BadRequestCopyWithImpl<T, _$BadRequest<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return badRequest(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return badRequest?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (badRequest != null) {
      return badRequest(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return badRequest(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return badRequest?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (badRequest != null) {
      return badRequest(this);
    }
    return orElse();
  }
}

abstract class BadRequest<T> implements ApiException<T> {
  const factory BadRequest(final T error) = _$BadRequest<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$BadRequestCopyWith<T, _$BadRequest<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ForbiddenCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$ForbiddenCopyWith(
          _$Forbidden<T> value, $Res Function(_$Forbidden<T>) then) =
      __$$ForbiddenCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$ForbiddenCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$Forbidden<T>>
    implements _$$ForbiddenCopyWith<T, $Res> {
  __$$ForbiddenCopyWithImpl(
      _$Forbidden<T> _value, $Res Function(_$Forbidden<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$Forbidden<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$Forbidden<T> implements Forbidden<T> {
  const _$Forbidden(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.forbidden(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Forbidden<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ForbiddenCopyWith<T, _$Forbidden<T>> get copyWith =>
      __$$ForbiddenCopyWithImpl<T, _$Forbidden<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return forbidden(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return forbidden?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (forbidden != null) {
      return forbidden(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return forbidden(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return forbidden?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (forbidden != null) {
      return forbidden(this);
    }
    return orElse();
  }
}

abstract class Forbidden<T> implements ApiException<T> {
  const factory Forbidden(final T error) = _$Forbidden<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$ForbiddenCopyWith<T, _$Forbidden<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NotFoundCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$NotFoundCopyWith(
          _$NotFound<T> value, $Res Function(_$NotFound<T>) then) =
      __$$NotFoundCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$NotFoundCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$NotFound<T>>
    implements _$$NotFoundCopyWith<T, $Res> {
  __$$NotFoundCopyWithImpl(
      _$NotFound<T> _value, $Res Function(_$NotFound<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$NotFound<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$NotFound<T> implements NotFound<T> {
  const _$NotFound(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.notFound(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NotFound<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NotFoundCopyWith<T, _$NotFound<T>> get copyWith =>
      __$$NotFoundCopyWithImpl<T, _$NotFound<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return notFound(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return notFound?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return notFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return notFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(this);
    }
    return orElse();
  }
}

abstract class NotFound<T> implements ApiException<T> {
  const factory NotFound(final T error) = _$NotFound<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$NotFoundCopyWith<T, _$NotFound<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$MethodNotAllowedCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$MethodNotAllowedCopyWith(_$MethodNotAllowed<T> value,
          $Res Function(_$MethodNotAllowed<T>) then) =
      __$$MethodNotAllowedCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$MethodNotAllowedCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$MethodNotAllowed<T>>
    implements _$$MethodNotAllowedCopyWith<T, $Res> {
  __$$MethodNotAllowedCopyWithImpl(
      _$MethodNotAllowed<T> _value, $Res Function(_$MethodNotAllowed<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$MethodNotAllowed<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$MethodNotAllowed<T> implements MethodNotAllowed<T> {
  const _$MethodNotAllowed(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.methodNotAllowed(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MethodNotAllowed<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MethodNotAllowedCopyWith<T, _$MethodNotAllowed<T>> get copyWith =>
      __$$MethodNotAllowedCopyWithImpl<T, _$MethodNotAllowed<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return methodNotAllowed(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return methodNotAllowed?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (methodNotAllowed != null) {
      return methodNotAllowed(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return methodNotAllowed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return methodNotAllowed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (methodNotAllowed != null) {
      return methodNotAllowed(this);
    }
    return orElse();
  }
}

abstract class MethodNotAllowed<T> implements ApiException<T> {
  const factory MethodNotAllowed(final T error) = _$MethodNotAllowed<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$MethodNotAllowedCopyWith<T, _$MethodNotAllowed<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NotAcceptableCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$NotAcceptableCopyWith(
          _$NotAcceptable<T> value, $Res Function(_$NotAcceptable<T>) then) =
      __$$NotAcceptableCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$NotAcceptableCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$NotAcceptable<T>>
    implements _$$NotAcceptableCopyWith<T, $Res> {
  __$$NotAcceptableCopyWithImpl(
      _$NotAcceptable<T> _value, $Res Function(_$NotAcceptable<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$NotAcceptable<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$NotAcceptable<T> implements NotAcceptable<T> {
  const _$NotAcceptable(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.notAcceptable(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NotAcceptable<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NotAcceptableCopyWith<T, _$NotAcceptable<T>> get copyWith =>
      __$$NotAcceptableCopyWithImpl<T, _$NotAcceptable<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return notAcceptable(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return notAcceptable?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (notAcceptable != null) {
      return notAcceptable(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return notAcceptable(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return notAcceptable?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (notAcceptable != null) {
      return notAcceptable(this);
    }
    return orElse();
  }
}

abstract class NotAcceptable<T> implements ApiException<T> {
  const factory NotAcceptable(final T error) = _$NotAcceptable<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$NotAcceptableCopyWith<T, _$NotAcceptable<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RequestTimeoutCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$RequestTimeoutCopyWith(
          _$RequestTimeout<T> value, $Res Function(_$RequestTimeout<T>) then) =
      __$$RequestTimeoutCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$RequestTimeoutCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$RequestTimeout<T>>
    implements _$$RequestTimeoutCopyWith<T, $Res> {
  __$$RequestTimeoutCopyWithImpl(
      _$RequestTimeout<T> _value, $Res Function(_$RequestTimeout<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$RequestTimeout<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$RequestTimeout<T> implements RequestTimeout<T> {
  const _$RequestTimeout(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.requestTimeout(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RequestTimeout<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RequestTimeoutCopyWith<T, _$RequestTimeout<T>> get copyWith =>
      __$$RequestTimeoutCopyWithImpl<T, _$RequestTimeout<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return requestTimeout(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return requestTimeout?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (requestTimeout != null) {
      return requestTimeout(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return requestTimeout(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return requestTimeout?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (requestTimeout != null) {
      return requestTimeout(this);
    }
    return orElse();
  }
}

abstract class RequestTimeout<T> implements ApiException<T> {
  const factory RequestTimeout(final T error) = _$RequestTimeout<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$RequestTimeoutCopyWith<T, _$RequestTimeout<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnprocessableEntityCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$UnprocessableEntityCopyWith(_$UnprocessableEntity<T> value,
          $Res Function(_$UnprocessableEntity<T>) then) =
      __$$UnprocessableEntityCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$UnprocessableEntityCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$UnprocessableEntity<T>>
    implements _$$UnprocessableEntityCopyWith<T, $Res> {
  __$$UnprocessableEntityCopyWithImpl(_$UnprocessableEntity<T> _value,
      $Res Function(_$UnprocessableEntity<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$UnprocessableEntity<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$UnprocessableEntity<T> implements UnprocessableEntity<T> {
  const _$UnprocessableEntity(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.unprocessableEntity(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnprocessableEntity<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UnprocessableEntityCopyWith<T, _$UnprocessableEntity<T>> get copyWith =>
      __$$UnprocessableEntityCopyWithImpl<T, _$UnprocessableEntity<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return unprocessableEntity(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return unprocessableEntity?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (unprocessableEntity != null) {
      return unprocessableEntity(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return unprocessableEntity(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return unprocessableEntity?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (unprocessableEntity != null) {
      return unprocessableEntity(this);
    }
    return orElse();
  }
}

abstract class UnprocessableEntity<T> implements ApiException<T> {
  const factory UnprocessableEntity(final T error) = _$UnprocessableEntity<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$UnprocessableEntityCopyWith<T, _$UnprocessableEntity<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SendTimeoutCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$SendTimeoutCopyWith(
          _$SendTimeout<T> value, $Res Function(_$SendTimeout<T>) then) =
      __$$SendTimeoutCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$SendTimeoutCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$SendTimeout<T>>
    implements _$$SendTimeoutCopyWith<T, $Res> {
  __$$SendTimeoutCopyWithImpl(
      _$SendTimeout<T> _value, $Res Function(_$SendTimeout<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$SendTimeout<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$SendTimeout<T> implements SendTimeout<T> {
  const _$SendTimeout(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.sendTimeout(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SendTimeout<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SendTimeoutCopyWith<T, _$SendTimeout<T>> get copyWith =>
      __$$SendTimeoutCopyWithImpl<T, _$SendTimeout<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return sendTimeout(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return sendTimeout?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (sendTimeout != null) {
      return sendTimeout(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return sendTimeout(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return sendTimeout?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (sendTimeout != null) {
      return sendTimeout(this);
    }
    return orElse();
  }
}

abstract class SendTimeout<T> implements ApiException<T> {
  const factory SendTimeout(final T error) = _$SendTimeout<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$SendTimeoutCopyWith<T, _$SendTimeout<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ConflictCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$ConflictCopyWith(
          _$Conflict<T> value, $Res Function(_$Conflict<T>) then) =
      __$$ConflictCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$ConflictCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$Conflict<T>>
    implements _$$ConflictCopyWith<T, $Res> {
  __$$ConflictCopyWithImpl(
      _$Conflict<T> _value, $Res Function(_$Conflict<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$Conflict<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$Conflict<T> implements Conflict<T> {
  const _$Conflict(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.conflict(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Conflict<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ConflictCopyWith<T, _$Conflict<T>> get copyWith =>
      __$$ConflictCopyWithImpl<T, _$Conflict<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return conflict(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return conflict?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (conflict != null) {
      return conflict(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return conflict(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return conflict?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (conflict != null) {
      return conflict(this);
    }
    return orElse();
  }
}

abstract class Conflict<T> implements ApiException<T> {
  const factory Conflict(final T error) = _$Conflict<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$ConflictCopyWith<T, _$Conflict<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InternalServerErrorCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$InternalServerErrorCopyWith(_$InternalServerError<T> value,
          $Res Function(_$InternalServerError<T>) then) =
      __$$InternalServerErrorCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$InternalServerErrorCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$InternalServerError<T>>
    implements _$$InternalServerErrorCopyWith<T, $Res> {
  __$$InternalServerErrorCopyWithImpl(_$InternalServerError<T> _value,
      $Res Function(_$InternalServerError<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$InternalServerError<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$InternalServerError<T> implements InternalServerError<T> {
  const _$InternalServerError(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.internalServerError(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InternalServerError<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$InternalServerErrorCopyWith<T, _$InternalServerError<T>> get copyWith =>
      __$$InternalServerErrorCopyWithImpl<T, _$InternalServerError<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return internalServerError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return internalServerError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (internalServerError != null) {
      return internalServerError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return internalServerError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return internalServerError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (internalServerError != null) {
      return internalServerError(this);
    }
    return orElse();
  }
}

abstract class InternalServerError<T> implements ApiException<T> {
  const factory InternalServerError(final T error) = _$InternalServerError<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$InternalServerErrorCopyWith<T, _$InternalServerError<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NotImplementedCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$NotImplementedCopyWith(
          _$NotImplemented<T> value, $Res Function(_$NotImplemented<T>) then) =
      __$$NotImplementedCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$NotImplementedCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$NotImplemented<T>>
    implements _$$NotImplementedCopyWith<T, $Res> {
  __$$NotImplementedCopyWithImpl(
      _$NotImplemented<T> _value, $Res Function(_$NotImplemented<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$NotImplemented<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$NotImplemented<T> implements NotImplemented<T> {
  const _$NotImplemented(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.notImplemented(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NotImplemented<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NotImplementedCopyWith<T, _$NotImplemented<T>> get copyWith =>
      __$$NotImplementedCopyWithImpl<T, _$NotImplemented<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return notImplemented(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return notImplemented?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (notImplemented != null) {
      return notImplemented(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return notImplemented(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return notImplemented?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (notImplemented != null) {
      return notImplemented(this);
    }
    return orElse();
  }
}

abstract class NotImplemented<T> implements ApiException<T> {
  const factory NotImplemented(final T error) = _$NotImplemented<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$NotImplementedCopyWith<T, _$NotImplemented<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ServiceUnavailableCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$ServiceUnavailableCopyWith(_$ServiceUnavailable<T> value,
          $Res Function(_$ServiceUnavailable<T>) then) =
      __$$ServiceUnavailableCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$ServiceUnavailableCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$ServiceUnavailable<T>>
    implements _$$ServiceUnavailableCopyWith<T, $Res> {
  __$$ServiceUnavailableCopyWithImpl(_$ServiceUnavailable<T> _value,
      $Res Function(_$ServiceUnavailable<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$ServiceUnavailable<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$ServiceUnavailable<T> implements ServiceUnavailable<T> {
  const _$ServiceUnavailable(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.serviceUnavailable(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ServiceUnavailable<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ServiceUnavailableCopyWith<T, _$ServiceUnavailable<T>> get copyWith =>
      __$$ServiceUnavailableCopyWithImpl<T, _$ServiceUnavailable<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return serviceUnavailable(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return serviceUnavailable?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (serviceUnavailable != null) {
      return serviceUnavailable(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return serviceUnavailable(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return serviceUnavailable?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (serviceUnavailable != null) {
      return serviceUnavailable(this);
    }
    return orElse();
  }
}

abstract class ServiceUnavailable<T> implements ApiException<T> {
  const factory ServiceUnavailable(final T error) = _$ServiceUnavailable<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$ServiceUnavailableCopyWith<T, _$ServiceUnavailable<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NoInternetConnectionCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$NoInternetConnectionCopyWith(_$NoInternetConnection<T> value,
          $Res Function(_$NoInternetConnection<T>) then) =
      __$$NoInternetConnectionCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$NoInternetConnectionCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$NoInternetConnection<T>>
    implements _$$NoInternetConnectionCopyWith<T, $Res> {
  __$$NoInternetConnectionCopyWithImpl(_$NoInternetConnection<T> _value,
      $Res Function(_$NoInternetConnection<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$NoInternetConnection<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$NoInternetConnection<T> implements NoInternetConnection<T> {
  const _$NoInternetConnection(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.noInternetConnection(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NoInternetConnection<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NoInternetConnectionCopyWith<T, _$NoInternetConnection<T>> get copyWith =>
      __$$NoInternetConnectionCopyWithImpl<T, _$NoInternetConnection<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return noInternetConnection(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return noInternetConnection?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (noInternetConnection != null) {
      return noInternetConnection(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return noInternetConnection(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return noInternetConnection?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (noInternetConnection != null) {
      return noInternetConnection(this);
    }
    return orElse();
  }
}

abstract class NoInternetConnection<T> implements ApiException<T> {
  const factory NoInternetConnection(final T error) = _$NoInternetConnection<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$NoInternetConnectionCopyWith<T, _$NoInternetConnection<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$FormatExceptionCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$FormatExceptionCopyWith(_$FormatException<T> value,
          $Res Function(_$FormatException<T>) then) =
      __$$FormatExceptionCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$FormatExceptionCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$FormatException<T>>
    implements _$$FormatExceptionCopyWith<T, $Res> {
  __$$FormatExceptionCopyWithImpl(
      _$FormatException<T> _value, $Res Function(_$FormatException<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$FormatException<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$FormatException<T> implements FormatException<T> {
  const _$FormatException(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.formatException(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FormatException<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FormatExceptionCopyWith<T, _$FormatException<T>> get copyWith =>
      __$$FormatExceptionCopyWithImpl<T, _$FormatException<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return formatException(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return formatException?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (formatException != null) {
      return formatException(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return formatException(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return formatException?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (formatException != null) {
      return formatException(this);
    }
    return orElse();
  }
}

abstract class FormatException<T> implements ApiException<T> {
  const factory FormatException(final T error) = _$FormatException<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$FormatExceptionCopyWith<T, _$FormatException<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnableToProcessCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$UnableToProcessCopyWith(_$UnableToProcess<T> value,
          $Res Function(_$UnableToProcess<T>) then) =
      __$$UnableToProcessCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$UnableToProcessCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$UnableToProcess<T>>
    implements _$$UnableToProcessCopyWith<T, $Res> {
  __$$UnableToProcessCopyWithImpl(
      _$UnableToProcess<T> _value, $Res Function(_$UnableToProcess<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$UnableToProcess<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$UnableToProcess<T> implements UnableToProcess<T> {
  const _$UnableToProcess(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.unableToProcess(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnableToProcess<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UnableToProcessCopyWith<T, _$UnableToProcess<T>> get copyWith =>
      __$$UnableToProcessCopyWithImpl<T, _$UnableToProcess<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return unableToProcess(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return unableToProcess?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (unableToProcess != null) {
      return unableToProcess(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return unableToProcess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return unableToProcess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (unableToProcess != null) {
      return unableToProcess(this);
    }
    return orElse();
  }
}

abstract class UnableToProcess<T> implements ApiException<T> {
  const factory UnableToProcess(final T error) = _$UnableToProcess<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$UnableToProcessCopyWith<T, _$UnableToProcess<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DefaultErrorCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$DefaultErrorCopyWith(
          _$DefaultError<T> value, $Res Function(_$DefaultError<T>) then) =
      __$$DefaultErrorCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$DefaultErrorCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$DefaultError<T>>
    implements _$$DefaultErrorCopyWith<T, $Res> {
  __$$DefaultErrorCopyWithImpl(
      _$DefaultError<T> _value, $Res Function(_$DefaultError<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$DefaultError<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$DefaultError<T> implements DefaultError<T> {
  const _$DefaultError(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.defaultError(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DefaultError<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DefaultErrorCopyWith<T, _$DefaultError<T>> get copyWith =>
      __$$DefaultErrorCopyWithImpl<T, _$DefaultError<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return defaultError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return defaultError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (defaultError != null) {
      return defaultError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return defaultError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return defaultError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (defaultError != null) {
      return defaultError(this);
    }
    return orElse();
  }
}

abstract class DefaultError<T> implements ApiException<T> {
  const factory DefaultError(final T error) = _$DefaultError<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$DefaultErrorCopyWith<T, _$DefaultError<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnexpectedErrorCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$UnexpectedErrorCopyWith(_$UnexpectedError<T> value,
          $Res Function(_$UnexpectedError<T>) then) =
      __$$UnexpectedErrorCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$UnexpectedErrorCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$UnexpectedError<T>>
    implements _$$UnexpectedErrorCopyWith<T, $Res> {
  __$$UnexpectedErrorCopyWithImpl(
      _$UnexpectedError<T> _value, $Res Function(_$UnexpectedError<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$UnexpectedError<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$UnexpectedError<T> implements UnexpectedError<T> {
  const _$UnexpectedError(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.unexpectedError(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnexpectedError<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UnexpectedErrorCopyWith<T, _$UnexpectedError<T>> get copyWith =>
      __$$UnexpectedErrorCopyWithImpl<T, _$UnexpectedError<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return unexpectedError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return unexpectedError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return unexpectedError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return unexpectedError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (unexpectedError != null) {
      return unexpectedError(this);
    }
    return orElse();
  }
}

abstract class UnexpectedError<T> implements ApiException<T> {
  const factory UnexpectedError(final T error) = _$UnexpectedError<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$UnexpectedErrorCopyWith<T, _$UnexpectedError<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$BadCerfiticateCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$BadCerfiticateCopyWith(
          _$BadCerfiticate<T> value, $Res Function(_$BadCerfiticate<T>) then) =
      __$$BadCerfiticateCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$BadCerfiticateCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$BadCerfiticate<T>>
    implements _$$BadCerfiticateCopyWith<T, $Res> {
  __$$BadCerfiticateCopyWithImpl(
      _$BadCerfiticate<T> _value, $Res Function(_$BadCerfiticate<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$BadCerfiticate<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$BadCerfiticate<T> implements BadCerfiticate<T> {
  const _$BadCerfiticate(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.badCertificate(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BadCerfiticate<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BadCerfiticateCopyWith<T, _$BadCerfiticate<T>> get copyWith =>
      __$$BadCerfiticateCopyWithImpl<T, _$BadCerfiticate<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return badCertificate(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return badCertificate?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (badCertificate != null) {
      return badCertificate(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return badCertificate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return badCertificate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (badCertificate != null) {
      return badCertificate(this);
    }
    return orElse();
  }
}

abstract class BadCerfiticate<T> implements ApiException<T> {
  const factory BadCerfiticate(final T error) = _$BadCerfiticate<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$BadCerfiticateCopyWith<T, _$BadCerfiticate<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ConnectionErrorCopyWith<T, $Res>
    implements $ApiExceptionCopyWith<T, $Res> {
  factory _$$ConnectionErrorCopyWith(_$ConnectionError<T> value,
          $Res Function(_$ConnectionError<T>) then) =
      __$$ConnectionErrorCopyWithImpl<T, $Res>;
  @override
  @useResult
  $Res call({T error});
}

/// @nodoc
class __$$ConnectionErrorCopyWithImpl<T, $Res>
    extends _$ApiExceptionCopyWithImpl<T, $Res, _$ConnectionError<T>>
    implements _$$ConnectionErrorCopyWith<T, $Res> {
  __$$ConnectionErrorCopyWithImpl(
      _$ConnectionError<T> _value, $Res Function(_$ConnectionError<T>) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$ConnectionError<T>(
      freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as T,
    ));
  }
}

/// @nodoc

class _$ConnectionError<T> implements ConnectionError<T> {
  const _$ConnectionError(this.error);

  @override
  final T error;

  @override
  String toString() {
    return 'ApiException<$T>.connectionError(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ConnectionError<T> &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ConnectionErrorCopyWith<T, _$ConnectionError<T>> get copyWith =>
      __$$ConnectionErrorCopyWithImpl<T, _$ConnectionError<T>>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(T error) requestCancelled,
    required TResult Function(T error) unauthorisedRequest,
    required TResult Function(T error) badRequest,
    required TResult Function(T error) forbidden,
    required TResult Function(T error) notFound,
    required TResult Function(T error) methodNotAllowed,
    required TResult Function(T error) notAcceptable,
    required TResult Function(T error) requestTimeout,
    required TResult Function(T error) unprocessableEntity,
    required TResult Function(T error) sendTimeout,
    required TResult Function(T error) conflict,
    required TResult Function(T error) internalServerError,
    required TResult Function(T error) notImplemented,
    required TResult Function(T error) serviceUnavailable,
    required TResult Function(T error) noInternetConnection,
    required TResult Function(T error) formatException,
    required TResult Function(T error) unableToProcess,
    required TResult Function(T error) defaultError,
    required TResult Function(T error) unexpectedError,
    required TResult Function(T error) badCertificate,
    required TResult Function(T error) connectionError,
  }) {
    return connectionError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(T error)? requestCancelled,
    TResult? Function(T error)? unauthorisedRequest,
    TResult? Function(T error)? badRequest,
    TResult? Function(T error)? forbidden,
    TResult? Function(T error)? notFound,
    TResult? Function(T error)? methodNotAllowed,
    TResult? Function(T error)? notAcceptable,
    TResult? Function(T error)? requestTimeout,
    TResult? Function(T error)? unprocessableEntity,
    TResult? Function(T error)? sendTimeout,
    TResult? Function(T error)? conflict,
    TResult? Function(T error)? internalServerError,
    TResult? Function(T error)? notImplemented,
    TResult? Function(T error)? serviceUnavailable,
    TResult? Function(T error)? noInternetConnection,
    TResult? Function(T error)? formatException,
    TResult? Function(T error)? unableToProcess,
    TResult? Function(T error)? defaultError,
    TResult? Function(T error)? unexpectedError,
    TResult? Function(T error)? badCertificate,
    TResult? Function(T error)? connectionError,
  }) {
    return connectionError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(T error)? requestCancelled,
    TResult Function(T error)? unauthorisedRequest,
    TResult Function(T error)? badRequest,
    TResult Function(T error)? forbidden,
    TResult Function(T error)? notFound,
    TResult Function(T error)? methodNotAllowed,
    TResult Function(T error)? notAcceptable,
    TResult Function(T error)? requestTimeout,
    TResult Function(T error)? unprocessableEntity,
    TResult Function(T error)? sendTimeout,
    TResult Function(T error)? conflict,
    TResult Function(T error)? internalServerError,
    TResult Function(T error)? notImplemented,
    TResult Function(T error)? serviceUnavailable,
    TResult Function(T error)? noInternetConnection,
    TResult Function(T error)? formatException,
    TResult Function(T error)? unableToProcess,
    TResult Function(T error)? defaultError,
    TResult Function(T error)? unexpectedError,
    TResult Function(T error)? badCertificate,
    TResult Function(T error)? connectionError,
    required TResult orElse(),
  }) {
    if (connectionError != null) {
      return connectionError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RequestCancelled<T> value) requestCancelled,
    required TResult Function(UnauthorisedRequest<T> value) unauthorisedRequest,
    required TResult Function(BadRequest<T> value) badRequest,
    required TResult Function(Forbidden<T> value) forbidden,
    required TResult Function(NotFound<T> value) notFound,
    required TResult Function(MethodNotAllowed<T> value) methodNotAllowed,
    required TResult Function(NotAcceptable<T> value) notAcceptable,
    required TResult Function(RequestTimeout<T> value) requestTimeout,
    required TResult Function(UnprocessableEntity<T> value) unprocessableEntity,
    required TResult Function(SendTimeout<T> value) sendTimeout,
    required TResult Function(Conflict<T> value) conflict,
    required TResult Function(InternalServerError<T> value) internalServerError,
    required TResult Function(NotImplemented<T> value) notImplemented,
    required TResult Function(ServiceUnavailable<T> value) serviceUnavailable,
    required TResult Function(NoInternetConnection<T> value)
        noInternetConnection,
    required TResult Function(FormatException<T> value) formatException,
    required TResult Function(UnableToProcess<T> value) unableToProcess,
    required TResult Function(DefaultError<T> value) defaultError,
    required TResult Function(UnexpectedError<T> value) unexpectedError,
    required TResult Function(BadCerfiticate<T> value) badCertificate,
    required TResult Function(ConnectionError<T> value) connectionError,
  }) {
    return connectionError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RequestCancelled<T> value)? requestCancelled,
    TResult? Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult? Function(BadRequest<T> value)? badRequest,
    TResult? Function(Forbidden<T> value)? forbidden,
    TResult? Function(NotFound<T> value)? notFound,
    TResult? Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult? Function(NotAcceptable<T> value)? notAcceptable,
    TResult? Function(RequestTimeout<T> value)? requestTimeout,
    TResult? Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult? Function(SendTimeout<T> value)? sendTimeout,
    TResult? Function(Conflict<T> value)? conflict,
    TResult? Function(InternalServerError<T> value)? internalServerError,
    TResult? Function(NotImplemented<T> value)? notImplemented,
    TResult? Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult? Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult? Function(FormatException<T> value)? formatException,
    TResult? Function(UnableToProcess<T> value)? unableToProcess,
    TResult? Function(DefaultError<T> value)? defaultError,
    TResult? Function(UnexpectedError<T> value)? unexpectedError,
    TResult? Function(BadCerfiticate<T> value)? badCertificate,
    TResult? Function(ConnectionError<T> value)? connectionError,
  }) {
    return connectionError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RequestCancelled<T> value)? requestCancelled,
    TResult Function(UnauthorisedRequest<T> value)? unauthorisedRequest,
    TResult Function(BadRequest<T> value)? badRequest,
    TResult Function(Forbidden<T> value)? forbidden,
    TResult Function(NotFound<T> value)? notFound,
    TResult Function(MethodNotAllowed<T> value)? methodNotAllowed,
    TResult Function(NotAcceptable<T> value)? notAcceptable,
    TResult Function(RequestTimeout<T> value)? requestTimeout,
    TResult Function(UnprocessableEntity<T> value)? unprocessableEntity,
    TResult Function(SendTimeout<T> value)? sendTimeout,
    TResult Function(Conflict<T> value)? conflict,
    TResult Function(InternalServerError<T> value)? internalServerError,
    TResult Function(NotImplemented<T> value)? notImplemented,
    TResult Function(ServiceUnavailable<T> value)? serviceUnavailable,
    TResult Function(NoInternetConnection<T> value)? noInternetConnection,
    TResult Function(FormatException<T> value)? formatException,
    TResult Function(UnableToProcess<T> value)? unableToProcess,
    TResult Function(DefaultError<T> value)? defaultError,
    TResult Function(UnexpectedError<T> value)? unexpectedError,
    TResult Function(BadCerfiticate<T> value)? badCertificate,
    TResult Function(ConnectionError<T> value)? connectionError,
    required TResult orElse(),
  }) {
    if (connectionError != null) {
      return connectionError(this);
    }
    return orElse();
  }
}

abstract class ConnectionError<T> implements ApiException<T> {
  const factory ConnectionError(final T error) = _$ConnectionError<T>;

  @override
  T get error;
  @override
  @JsonKey(ignore: true)
  _$$ConnectionErrorCopyWith<T, _$ConnectionError<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
