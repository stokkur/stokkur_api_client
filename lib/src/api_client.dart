import 'package:dio/dio.dart';
import 'package:stokkur_api_client/stokkur_api_client.dart';

class ApiClient {
  ApiClient._privateConstructor();
  static final ApiClient _instance = ApiClient._privateConstructor();
  static ApiClient get instance => _instance;

  late Dio dio;
  late ApiErrorConverter errorConverter;

  @Deprecated('This constructor is deprecated, you should use the new constructor [ApiClient.fromDio] instead')
  factory ApiClient({
    required String baseUrl,
    required Map<String, Object> headers,
    List<Interceptor>? interceptors,
    ApiErrorConverter? errorConverter,
    Duration? connectTimeout,
    Duration? receiveTimeout,
  }) {
    _instance.dio = Dio(BaseOptions(
      baseUrl: baseUrl,
      headers: headers,
      connectTimeout: connectTimeout,
      receiveTimeout: receiveTimeout,
    ));
    if (interceptors != null) {
      _instance.dio.interceptors.addAll(interceptors);
    }
    _instance.errorConverter = errorConverter ?? DefaultErrorConverter();
    return _instance;
  }

  factory ApiClient.fromDio({
    required Dio dio,
    ApiErrorConverter? errorConverter,
  }) {
    _instance.dio = dio;
    _instance.errorConverter = errorConverter ?? DefaultErrorConverter();
    return _instance;
  }

  /// Helper method to handle the request response and errors
  Future<ApiResult<T>> request<T, E>(
    ApiRequest request,
    Function(dynamic) fromJson, {
    bool showGenericError = true,
  }) async {
    try {
      final response = await _instance.dio.request(
        request.url,
        data: request.body,
        queryParameters: request.query,
        options: Options(method: request.method, headers: request.headers),
      );
      return ApiResult.success(data: fromJson(response.data));
    } catch (e) {
      final exception = ApiException.getDioException<E>(e, _instance.errorConverter);
      return ApiResult.failure(error: exception);
    }
  }

  Future<String?> requestUri(String uri, Dio httpClient) async {
    final response = await httpClient.requestUri(Uri.parse(uri));

    return response.data;
  }

  void addHeaders(Map<String, dynamic> headers) {
    _instance.dio.options.headers.addAll(headers);
  }

  void removeHeader(String headerKey) {
    _instance.dio.options.headers.remove(headerKey);
  }
}
