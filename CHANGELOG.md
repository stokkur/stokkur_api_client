## 0.0.1
* The beta release

## 0.0.2
* Fix packages imports

## 0.0.3
* Accept error converter parameter

## 0.0.4
* Update freezed and support timeout parameters

## 0.0.5
* Add configuration parameters

## 0.0.6
* Fix crash logging file from the body

## 0.0.7
* Fix logging POST with body

## 0.0.8
* Improved error handling on encoding request body

## 0.0.9
* Deprecated default constructor ApiClient, should use ApiClient.fromDio instead
## 0.0.10
* Update dependencies
## 0.0.11
* Expose ErrorConverter base
## 0.0.12
* Upgrade Flutter to 3.10 and Dio 5