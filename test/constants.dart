class Constants {
  static const String baseUrl = 'https://jsonplaceholder.typicode.com';
  static const Map<String, Object> defaultHeader = {'Content-Type': 'application/json'};
}
