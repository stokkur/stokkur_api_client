import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:stokkur_api_client/stokkur_api_client.dart';

import 'constants.dart';

void main() {
  test('Api client is a singleton', () {
    const String otherUrl = 'https://mockjsonplaceholder.mock.com';

    final Dio dio = Dio(BaseOptions(baseUrl: otherUrl, headers: Constants.defaultHeader));
    final apiClient1 = ApiClient.fromDio(dio: dio);

    expect(apiClient1.dio.options.baseUrl, Constants.baseUrl);
    expect(apiClient1.dio.interceptors, isEmpty);

    final Dio dio2 = Dio(BaseOptions(baseUrl: otherUrl, headers: Constants.defaultHeader));
    dio2.interceptors.add(NetworkLoggerInterceptor());
    final apiClient2 = ApiClient.fromDio(dio: dio2);

    expect(apiClient2.dio.options.baseUrl, otherUrl);
    expect(apiClient2.dio.interceptors, isNotEmpty);

    expect(apiClient1.dio.options.baseUrl, otherUrl);
    expect(apiClient1.dio.interceptors, isNotEmpty);

    // This is the only test that matters:
    expect(apiClient1, apiClient2);
  });
}
