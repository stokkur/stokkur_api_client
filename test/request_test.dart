import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:stokkur_api_client/stokkur_api_client.dart';

import 'constants.dart';
import 'request_test.mocks.dart';

@GenerateMocks([HttpClientAdapter])
void main() {
  late final ApiClient apiClient;
  late final MockHttpClientAdapter mockHttpClientAdapter;

  setUp(() {
    final Dio dio = Dio(BaseOptions(baseUrl: Constants.baseUrl, headers: Constants.defaultHeader));
    mockHttpClientAdapter = MockHttpClientAdapter();
    dio.httpClientAdapter = mockHttpClientAdapter;
    apiClient = ApiClient.fromDio(dio: dio);

    apiClient.dio.httpClientAdapter = mockHttpClientAdapter;
  });

  test('can call to get response', () async {
    final responsepayload = jsonEncode({"response_code": "1000"});
    final httpResponse = ResponseBody.fromString(
      responsepayload,
      200,
      headers: {
        Headers.contentTypeHeader: [Headers.jsonContentType],
      },
    );

    final request = ApiRequest.get(Constants.baseUrl);

    when(mockHttpClientAdapter.fetch(
      RequestOptions(path: Constants.baseUrl),
      any,
      any,
    )).thenAnswer((_) async => httpResponse);

    final response = await apiClient.request(request, (json) => json);
    // final expected = {"response_code": "1000"};
    log(response.toString());

    // expect(response, equals(expected));
  });
}
