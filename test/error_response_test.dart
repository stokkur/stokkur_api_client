import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:stokkur_api_client/stokkur_api_client.dart';

import 'constants.dart';
import 'error_response_test.mocks.dart';

@GenerateMocks([HttpClientAdapter])
void main() {
  late final ApiClient apiClient;
  late final MockHttpClientAdapter mockHttpClientAdapter;

  setUpAll(() {
    final Dio dio = Dio(BaseOptions(baseUrl: Constants.baseUrl, headers: Constants.defaultHeader));
    dio.httpClientAdapter = mockHttpClientAdapter;
    apiClient = ApiClient.fromDio(dio: dio);
    mockHttpClientAdapter = MockHttpClientAdapter();
    apiClient.dio.httpClientAdapter = mockHttpClientAdapter;
  });

  test("Default converter returns string from a map", () async {
    final request = ApiRequest.get(Constants.baseUrl);
    // final result = await client.request(ApiRequest.get(), (p0) => null)

    final error = {"error": "this is a map error message"};

    when(mockHttpClientAdapter.fetch(
      any,
      any,
      any,
    )).thenThrow(DioError(
        requestOptions: RequestOptions(path: Constants.baseUrl),
        error: error,
        type: DioErrorType.badResponse,
        response: Response(
          requestOptions: RequestOptions(path: Constants.baseUrl),
          statusCode: 400,
          data: error,
        )));

    final response = await apiClient.request(request, (p0) => p0);
    log(response.toString());
    response.when(
      success: (_) => throw TestFailure("Thrown DioError should not result in a success"),
      failure: (e) => expect(e.error, isA<String>()),
    );
  });

  test("Default converter returns string from a string", () async {
    final request = ApiRequest.get(Constants.baseUrl);
    // final result = await client.request(ApiRequest.get(), (p0) => null)

    const error = "this is an error message";

    when(mockHttpClientAdapter.fetch(
      any,
      any,
      any,
    )).thenThrow(DioError(
        requestOptions: RequestOptions(path: Constants.baseUrl),
        error: error,
        type: DioErrorType.badResponse,
        response: Response(
          requestOptions: RequestOptions(path: Constants.baseUrl),
          statusCode: 400,
          data: error,
        )));

    final response = await apiClient.request(request, (p0) => p0);
    log(response.toString());
    response.when(
      success: (_) => throw TestFailure("Thrown DioError should not result in a success"),
      failure: (e) => expect(e.error, isA<String>()),
    );
  });
}
